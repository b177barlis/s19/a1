const getCube = Math.pow(2,3);

console.log(`The cube of 2 is ${getCube}`);

const address = [`258`, `Washington Ave`, `NW`, `California`, `90011`];
const [number, street, area, state, zipCode] = address;
console.log(`I live at ${number} ${street} ${area} ${state} ${zipCode}`);

const animal = {
	name: `Lolong`,
	type: `saltwater crocodile`,
	weight: `1075 kg`,
	measurement: `20 ft 3 in`
}

const {name, type, weight, measurement} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);


const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));

const reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
myDog.name = `Frankie`;
myDog.age = 5;
myDog.breed = `Miniature Dachshund`;

console.log(myDog);